﻿<%@ Page Title="HTML practice" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="html-practice.aspx.cs" Inherits="ContentandMasterPages.html_practice" %>

<asp:Content ID="content_col1" ContentPlaceHolderID="quad2" runat="server" height="300px">
        <h3>A head tag in HTML</h3>
        <ASP:CodeBox runat="server" ID="CodeBox2" ></ASP:CodeBox>
</asp:Content>

<asp:Content ID="content_col2" ContentPlaceHolderID="quad1" runat="server">
        <h3>A tricky concept</h3>
        <p>Using the head tag to link CSS, set the title, set the charset, and import a font.</p>
</asp:Content>


<asp:Content ID="content_col3" ContentPlaceHolderID="quad3" runat="server">
        <h3>Example code</h3>
        <p>
            A head tag starts every HTML file and they can have many useful tags as you can see here.
       </p>
</asp:Content>


<asp:Content ID="content_col4" ContentPlaceHolderID="quad4" runat="server">
        <h3>Helpful resources</h3>
        <ul>
            <li>Stack Exchange</li>
            <li>W3Schools HTML tutorials</li>
            <li>W3Schools CSS tutorials</li>
        </ul>
</asp:Content>