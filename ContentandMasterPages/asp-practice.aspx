﻿<%@ Page Title="ASP Practice" Language="C#" MasterPageFile="~/Site.Master" EnableViewState="true" AutoEventWireup="true" CodeBehind="asp-practice.aspx.cs" Inherits="ContentandMasterPages.asp_practice" %>

<asp:Content ID="content_col1" ContentPlaceHolderID="quad2" runat="server">
    <!--We need to create a new ID in the master page and put a DataGrid into a content control-->
    <!---->
    <h3>Example code</h3>
         
    <asp:CodeBox runat="server" ID="myfrontendcode" CodeToServe="my_asp"></asp:CodeBox>
</asp:Content>

<asp:Content ID="content_col2" ContentPlaceHolderID="quad1" runat="server">
        <h3>A tricky concept</h3>
        <p>Using a loop to add code rows to a code box.</p>
</asp:Content>


<asp:Content ID="content_col3" ContentPlaceHolderID="quad3" runat="server">
        <h3>The concept</h3>
        <p>For every row, we add an empty row to the table and insert the row into that empty row. We change the column name
            which causes the text inside that row to change to the desired text.
        </p>
</asp:Content>

<asp:Content ID="content_col4" ContentPlaceHolderID="quad4" runat="server">
        <h3>Helpful resources</h3>
        <ul>
            <li>W3Schools</li>
            <li>Microsoft resources</li>
            <li>Stack Exchange</li>
        </ul>
</asp:Content>