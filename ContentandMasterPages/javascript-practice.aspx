﻿<%@ Page Title="Javascript practice" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="javascript-practice.aspx.cs" Inherits="ContentandMasterPages.javascript_practice" %>

<asp:Content ID="content_col1" ContentPlaceHolderID="quad2" runat="server">
    <h3>Example of an if statement</h3>
        <asp:CodeBox runat="server" ID="myjs"></ASP:CodeBox>    
</asp:Content>

<asp:Content ID="content_col2" ContentPlaceHolderID="quad1" runat="server">
        <h3>A tricky concept</h3>
        <p>This is how you use IF statements!</p>
</asp:Content>

<asp:Content ID="content_col3" ContentPlaceHolderID="quad3" runat="server">
        <h3>The concept</h3>
        <p>
            IF statements can be used to control the order which the program executes statements. First the condition in the
            heading brackets is evalutated. If true, everything in the { brackets is executed. If false, the code is skipped over.
        </p>
</asp:Content>


<asp:Content ID="content_col4" ContentPlaceHolderID="quad4" runat="server">
        <h3>Helpful resources</h3>
        <ul>
            <li>W3Schools Javascript resources</li>
            <li>Stack Exchange</li>
            <li>AAAND Google</li>
        </ul>
</asp:Content>