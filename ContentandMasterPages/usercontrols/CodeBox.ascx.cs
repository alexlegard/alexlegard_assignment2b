﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace ContentandMasterPages
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        
        DataView CreateView()
        {
            var page = (Page)HttpContext.Current.CurrentHandler;
            string url = page.AppRelativeVirtualPath;
            //debug.InnerHtml = url;
        
            //Create the data table
            DataTable table = new DataTable();

            //Create the line number column
            DataColumn lineCol = new DataColumn();

            //Create the code column
            DataColumn codeCol = new DataColumn();

            //Also create a row
            DataRow row;

            //Set the properties of the line number column
            lineCol.ColumnName = "Line";
            lineCol.DataType = System.Type.GetType("System.Int32");

            //Add the line number column to the data table object
            table.Columns.Add(lineCol);

            //Set the properties of the code column
            codeCol.ColumnName = "Code";
            codeCol.DataType = System.Type.GetType("System.String");

            //Add the code column to the data table object
            table.Columns.Add(codeCol);

            List<string> code;

            if (url== "~/asp-practice.aspx")
            {
                code = new List<string>(new String[] {
                    "int i = 0;",
                    "foreach(string code_line in front_end_code)",
                    "{",
                    "~coderow = codedata.NewRow();",
                    "~coderow[idx_col.ColumnName] = i;",
                    "~string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);",
                    "~formatted_code = formatted_code.Replace(\"~\", \"&nbsp; &nbsp; &nbsp; \");",
                    "~coderow[code_col.ColumnName] = formatted_code;",
                    "~i++;",
                    "~codedata.Rows.Add(coderow);",
                    "}"
                });
            }
            else if(url== "~/html-practice.aspx")
            {
                code= new List<string>(new String[] {
                    "<head>",
                    "<meta charset=\"utf-8\">",
                    "<title>World Traveler</title>",
                    "<link rel=\"stylesheet\" type=\"text/css\" href=\"week3style.css\">",
                    "<link href=\"https://fonts.googleapis.com/css?family=Covered+By+Your+Grace\" rel=\"stylesheet\">",
                    "</head>"
                });

            }else if (url == "~/javascript-practice.aspx")
            {
                code = new List<string>(new String[] {
                    "if(firstName == \"Sean\"){",
                    "alert(\"You are Sean\");",
                    "} else if(firstName == \"Bernie\") {",
                    "alert(\"You are Bernie\");",
                    "} else {",
                    "alert(\"I don't know your name.\")",
                    "}"
                });
            }
            else
            {
                code = new List<string>(new String[] { "No Code was found" });
            }

            //Create a list of strings, each string in the list is a line of code.
            
        //Create a list of strings, each string in the list is a line of code.
       
        int i = 0;
            foreach (string line in code)
            {
                row = table.NewRow();//Add row to the table
                row[lineCol] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                row[codeCol.ColumnName] = formatted_code;
                i++;
                table.Rows.Add(row);
            }
            //Create a code view using the data table we built then return the code view
            DataView view = new DataView(table);
            return view;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView view = CreateView();

            code_container.DataSource = view;
            code_container.DataBind();
        }
        
    }
    
}