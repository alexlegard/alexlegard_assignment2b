﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="true" CodeBehind="CodeBox.ascx.cs" Inherits="ContentandMasterPages.CodeBox" %>
<asp:DataGrid runat="server" ID="code_container" CssClass="code" Width="500px" CellPadding="2">
    <HeaderStyle Font-Size="Large" BackColor="#000" ForeColor ="White"/>
    <ItemStyle BackColor ="#ffffff" ForeColor ="#000000"/>
</asp:DataGrid>
<div id="debug" runat="server"></div>